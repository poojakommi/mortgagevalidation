package com.hackathon.mortgage.serviceinterface;

import org.springframework.http.ResponseEntity;

import com.hackathon.mortgage.dto.RequestDto;
import com.hackathon.mortgage.dto.ResponseDto;
import com.hackathon.mortgage.exception.InvalidApplication;

public interface MortgageServiceInterface {
	public ResponseEntity<ResponseDto> checkValidity(RequestDto requestDto) throws InvalidApplication ;

}
