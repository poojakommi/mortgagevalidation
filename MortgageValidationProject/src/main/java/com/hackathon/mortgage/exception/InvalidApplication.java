package com.hackathon.mortgage.exception;

public class InvalidApplication extends Exception {
	
	private static final long serialVersionUID = 1L;
	String message;
	
	public InvalidApplication(final String message)
	{
		super(message);
	}

}
