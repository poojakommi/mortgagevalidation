package com.hackathon.mortgage.service;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.hackathon.mortgage.dto.RequestDto;
import com.hackathon.mortgage.dto.ResponseDto;
import com.hackathon.mortgage.entity.City;
import com.hackathon.mortgage.exception.InvalidApplication;
import com.hackathon.mortgage.repository.CityRepository;
import com.hackathon.mortgage.repository.TierRepository;
import com.hackathon.mortgage.serviceinterface.MortgageServiceInterface;

@Service
public class MortgageService implements MortgageServiceInterface {

	@Autowired
	CityRepository cityRepo;

	@Autowired
	TierRepository tierRepo;

	
	Logger log = Logger.getLogger(MortgageService.class);
	
	/**
	 * @author pooja.kommi
	 * @param requestDto
	 * @return ResponseEntity<ResponseDto>
	 * One can check if they are eligible to apply for a loan by submitting their details
	 * through an application. This service method is to validate if they are eligible for 
	 * the loan or not.
	 */
	public ResponseEntity<ResponseDto> checkValidity(RequestDto requestDto) throws InvalidApplication {
		ResponseDto responseDto = new ResponseDto();

		City cityFromPincode = cityRepo.findAllByPincode(requestDto.getPincode());
		Long percentage = cityFromPincode.getTier().getPercentage();
		Double monthlyIncome = requestDto.getMonthlyIncome();
		Double monthyExpense = requestDto.getMonthlyExpenses();
		Double maxLoanAmount = ((percentage * (monthlyIncome - monthyExpense)) / 100);


		int duration = requestDto.getDurationOfMortgage();
		Long expectedLoan = requestDto.getExpectedMortgageAmount();

		if ((expectedLoan / (12 * duration)) > (maxLoanAmount)) {
			log.error("User entered details for for which loan is not available!");
			throw new InvalidApplication("Invalid Exception!!!");

		} else
			responseDto.setMessage("You can apply for Loan!");
		responseDto.setStatusCode(HttpStatus.OK.value());
		log.info("User entered details for for which loan is available!");
		return new ResponseEntity<>(responseDto, HttpStatus.OK);
	}

}
