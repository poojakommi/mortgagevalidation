package com.hackathon.mortgage.MortgageValidationProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.client.RestTemplate;

@EntityScan("com.hackathon.mortgage.entity")
@EnableJpaRepositories("com.hackathon.mortgage.repository")
@ComponentScan("com.hackathon.mortgage")
@SpringBootApplication
public class MortgageValidationProjectApplication {

	@Bean
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}
	
	public static void main(String[] args) {
		SpringApplication.run(MortgageValidationProjectApplication.class, args);
	}

}
