package com.hackathon.mortgage.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hackathon.mortgage.entity.Tier;

public interface TierRepository extends JpaRepository<Tier, Integer> {

}
