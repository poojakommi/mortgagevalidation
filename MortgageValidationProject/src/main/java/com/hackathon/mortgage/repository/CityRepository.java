package com.hackathon.mortgage.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hackathon.mortgage.entity.City;

@Repository
public interface CityRepository extends JpaRepository<City, Integer>{
public City findAllByPincode(Long pincode);
}
