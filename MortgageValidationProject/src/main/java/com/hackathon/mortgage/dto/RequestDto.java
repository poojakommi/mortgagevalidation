package com.hackathon.mortgage.dto;

import java.util.Date;

public class RequestDto {
private Date dateOfBirth;
public Date getDateOfBirth() {
	return dateOfBirth;
}
public void setDateOfBirth(Date dateOfBirth) {
	this.dateOfBirth = dateOfBirth;
}
public Double getMonthlyIncome() {
	return monthlyIncome;
}
public void setMonthlyIncome(Double monthlyIncome) {
	this.monthlyIncome = monthlyIncome;
}
public Double getMonthlyExpenses() {
	return MonthlyExpenses;
}
public void setMonthlyExpenses(Double monthlyExpenses) {
	MonthlyExpenses = monthlyExpenses;
}
public Long getPincode() {
	return pincode;
}
public void setPincode(Long pincode) {
	this.pincode = pincode;
}
public int getDurationOfMortgage() {
	return durationOfMortgage;
}
public void setDurationOfMortgage(int durationOfMortgage) {
	this.durationOfMortgage = durationOfMortgage;
}
public Long getExpectedMortgageAmount() {
	return expectedMortgageAmount;
}
public void setExpectedMortgageAmount(Long expectedMortgageAmount) {
	this.expectedMortgageAmount = expectedMortgageAmount;
}
private Double monthlyIncome;
private Double MonthlyExpenses;
private Long pincode;
private int durationOfMortgage;
private Long expectedMortgageAmount;

}
