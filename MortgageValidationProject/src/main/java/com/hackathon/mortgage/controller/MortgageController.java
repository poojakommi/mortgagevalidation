package com.hackathon.mortgage.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hackathon.mortgage.dto.RequestDto;
import com.hackathon.mortgage.dto.ResponseDto;
import com.hackathon.mortgage.exception.InvalidApplication;
import com.hackathon.mortgage.service.MortgageService;

@RestController
public class MortgageController {

	@Autowired
	MortgageService mortgageService;
	
	@PostMapping("/mortgages")
	public ResponseEntity<ResponseDto> checkValidity(@RequestBody RequestDto requestDto) throws InvalidApplication
	{
	   return mortgageService.checkValidity(requestDto);
	}
	
	
}
