package com.hackathon.mortgage.controller;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.hackathon.mortgage.dto.RequestDto;
import com.hackathon.mortgage.dto.ResponseDto;
import com.hackathon.mortgage.exception.InvalidApplication;
import com.hackathon.mortgage.serviceinterface.MortgageServiceInterface;


@RunWith(MockitoJUnitRunner.class)
public class MortgageControllerTest {

	@Mock
	MortgageServiceInterface mortgageService;
	
	@InjectMocks
	MortgageController mortgageController;
	
	RequestDto requestDto;
	ResponseDto responseDto;
	ResponseEntity<ResponseDto> result;
	Long pincode;

	
	@Before
	public void doBefore()
	{
		requestDto= new RequestDto();
		pincode = 560041L;
		requestDto.setPincode(pincode);
		
		responseDto = new ResponseDto();
		responseDto.setMessage("OK");
		responseDto.setStatusCode(200);
		result = new ResponseEntity<>(responseDto,HttpStatus.OK);
	}
	
	
	@Test
	public void validate() throws InvalidApplication
	{
		Mockito.when(mortgageService.checkValidity(requestDto)).thenReturn(result);
		ResponseEntity<ResponseDto> actualValue = mortgageController.checkValidity(requestDto);
		System.out.println(actualValue.getStatusCodeValue());
		Assert.assertEquals(200,actualValue.getStatusCode());
	}
	
}
